# README #

* Downloads -> Download repository
* Right click ->Extract all and rename 2nd, innermost folder, to redemptions
* Drop this folder, now named redemptions, into C:\ProgramData\Microsoft\Windows\Start Menu\Programs\AnkhHeart\AnkhBotR2\
* Create commands named !redeem, !handjive, !bassplay, and !degrees
* Copy the text in each command's text file and paste into the corresponding command's response box
* Change phrases and words to suit taste
* Each redemption will save a line in redeemed.log with date and time.